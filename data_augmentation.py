#!/usr/bin/env python
from glob import glob
from random import randint
import pandas as pd
import numpy as np
import pdb
import sys
import itertools

CLASSES={'Gender '       :  ( 'MALE','FEMALE'),
  '  Expressions' :  ('ANGER', 'HAPPINESS', 'SADNESS', 'SURPRISE', 'FEAR', 'DISGUST','NEUTRAL'),
  '  Occlusion  '  :   ('GLASSES','BEARD', 'ORNAMENTS', 'HAIR', 'HAND','NONE','OTHERS'),
  '  Illumination'  :   ('BAD', 'MEDIUM', 'HIGH'),
  '  Age         '    :   ('CHILD', 'YOUNG','MIDDLE' , 'OLD'),
  '  Pose        '    :   ('FRONTAL','LEFT','RIGHT','UP', 'DOWN')   ,
  '  Makeup      ' :  ('PARTIAL','OVER')}

def convert_csv(txt_file):
	file=[]
	with open(txt_file)	 as o:
		for i in list(o.readlines()):
			jj=i
			print(','.join(jj.split()))
			
			file.append(jj.split())

			
	f=pd.DataFrame(file)
	
	file_name=txt_file.replace('.txt','mod.txt')
	f.to_csv(file_name)

	return file_name

			
def main(path2,path):
	
	txt_files=glob(path)
	for i in txt_files:
		m=convert_csv(i)
	with open(path2) as f:
		files=list(f.readlines())
	
	rect=np.load('rect_file_path3.npy')
	f={}
	targets=[]
	rectangles=[]
	for idx,i in enumerate(files):
		k=i.split('/')[:-2]
		k.append('*mod.txt')
		txt_file=glob('/'.join(k))
		if(not(txt_file)):
			print("No annotation file at {}".format('/'.join(k)))
			#need to improve efficiency
			continue
		
			#Due to unreliable delimiters 
			#combine converssion with search later
		image=i.split('/')[-1].strip()
		
		try:
			annotations=pd.read_table(txt_file[0],delimiter=',',prefix='C',header=None)
		except Exception as e:
			annotations=pd.read_table(txt_file[0],delimiter=',',prefix='C',header=None,engine='c')
			if not(annotations):
				print('Ignoring file: {} \n Unable to read it'.format(txt_file[0]))
				idx+=1
				
		sorted_annotations=annotations.sort_values('C3')
		lists=sorted_annotations['C3'].tolist()
		# 
		if(image in lists):
			index=lists.index(image)
		else:
			# raise KeyError("Image not in text file")
			print("{} not in {}".format(image,lists))
		
		f[image]=annotations.iloc[index,11:].tolist()
		index=0
		
		# k=annotations.loc[annotations['C2'].values[:]==image]
		
		# f[image]=k[9:]
		cl=[]
		
		classes=[list(i) for i in CLASSES.values()]
		for i in classes:
			for j in i:
				cl.append(j)

		targets=[1 if i in f[image] else 0 for i in cl]
		
		f[image]=targets
		rectangles.append(rect[idx])
		idx+=1
	file=pd.DataFrame.from_dict(f,orient='index')
	file.to_csv('data_196_245.csv')
	np.save('rect_file_path5.npy',np.array(rectangles))
		# mod_anno={i[1]:i[9:] for i in annotations}
		# for k,v in mod_anno.items():



if __name__ == '__main__':
	# cl=[]
		
	# classes=[list(i) for i in CLASSES.values()]
	# for i in classes:
	# 	for j in i:
	# 		cl.append(j)
	# print(cl)
	# print(CLASSES.items())	

	# sys.exit(0)
	main(path2='file_paths3.txt',path='/home/bitcurian/pytorch/face_detection/IMFDB_final/*/*/*.txt')