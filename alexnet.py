import torch.nn as nn
import torch.utils.model_zoo as model_zoo
import numpy as np
import torch
import torch.nn.functional as F
from box_search import FRP_input
import pixel_sampling
import pdb

# from pixel_sampling import rectangle
__all__ = ['AlexNet', 'alexnet']


model_urls = {
    'alexnet': 'https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth',
}


class AlexNet_FRP(nn.Module):

    def __init__(self,batch_size,mean,std,image_height=224,image_width=224,num_attributes=None,num_attribute_values=None,classes_set_non_uniform=False, num_classes=29):
        super(AlexNet_FRP, self).__init__()
        self.height=image_height
        self.width=image_width
        self.num_classes=num_classes
        self.batch_size=batch_size
        self.mean=mean
        self.std=std

        self.features = nn.Sequential(
            nn.Conv2d(3, 96, kernel_size=5, stride=4, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(96, 256, kernel_size=3, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(256, 384, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3,stride=2),
            nn.Conv2d(384, 384, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3,stride=2),
            nn.Conv2d(384, 256, kernel_size=3, padding=1),
            #FRP PASS
#initialise these layers with the parameters of pre-trained AlexNet from alexnet_original
        )
        self.box=FRP_input()
        if(classes_set_non_uniform==False):
            self.loss_layers=self.original_post_fc_layers(num_attributes,num_attribute_values)
        else:
            self.loss_layers=self.custom_post_fc_layers(classes_set_non_uniform)
        self.classifier = nn.Sequential(
            nn.Dropout(),
            nn.Linear(2304, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(),
            nn.Linear(4096, 4096),
            nn.ReLU(inplace=True),
            nn.Linear(4096, 1024),#dimensionality reduction at the last layers are better off gradually done
        )

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
    

    @staticmethod
    def norm(coords,H,W):
        return (coords[0]/W,coords[1]/W,coords[2]/H,coords[3]/H)

    @staticmethod
    def clip_values (val, maxval_h,maxval_w):
        for i in val:
            if i[0]>=maxval_h:
                i[0]= maxval_h
            if i[1]>=maxval_w:
                i[1]= maxval_w    
        
        return val
    def frp_layer(self,x,boxes):
        #boxes={r1:(x1,y1,x2,y2),r2:(x1,y1,x2,y2)}
        #x,shape=(N,C,H,W)
        batch_feature_maps=[]
        for idx,i in enumerate(boxes):

            feature_maps=[]
            i={k:AlexNet_FRP.norm(v,self.height,self.width) for k,v in i.items()}
            H,W=x.shape[2],x.shape[3]
            for k,(x1,y1,x3,y3) in i.items():
                y2,x2=y1,x3
                y4,x4=y3,x1 
                # 
                pixels=pixel_sampling.get_pixels(tl=(x1*W,y1*H),tr=(x2*W,y2*H),br=(x3*W,y3*H),bl=(x4*W,y4*H))
                pixels=AlexNet_FRP.clip_values(pixels,H,W)
                part=[]
                # region=x[idx,:,int(y1*H):int(y3*H),int(x1*W):int(x3*W)]
                for p in pixels:
                    part.append(x[idx,:,p[1],p[0]].unsqueeze(1))
                    # 

                region=torch.cat(part,dim=1).unsqueeze(0)
                
                # 
                if(len(pixels)>1):
                    feature_maps.append(F.adaptive_max_pool1d(region,1))
                else:
                    feature_maps.append(region)
            feature_maps=torch.cat(feature_maps,dim=1)
            batch_feature_maps.append(feature_maps)
        # 
        return torch.cat(batch_feature_maps,0)


    def original_post_fc_layers(self,num_attributes,num_attribute_values):
        sub_module=[]
        for i in range(num_attributes):
            sub_module.append([nn.Linear(1024,num_attribute_values)])
        return nn.ModuleList(sub_module)
    def custom_post_fc_layers(self,classes):
        sub_module=[]
        for attribute,values in classes.items():
            sub_module.append(nn.Linear(1024,len(values)))
        return nn.ModuleList(sub_module)

    @staticmethod
    def denorm( tensor,mean, std):
        for t, m, s in zip(tensor, mean, std):
            t.mul_(s).add_(m)
            
        return 255.0*tensor

#define the FRP layersAlexNet_FRP.denorm(x.data.numpy()).astype(np.uint8)
    def forward(self, x,rect_preload):
        
        # x_denorm=AlexNet_FRP.denorm(x.clone().cpu(),mean=self.mean,std=self.std)
        #might not work on cpu() and cuda() simul. : check efficiency

        # rects=self.box.get_rects(np.transpose(np.array(x_denorm).astype(np.uint8)[:,:,:,::-1],(0,2,3,1)),batch_size=self.batch_size)
        

        rects=self.box.get_rects_preload(rects_preload=rect_preload,batch_size=self.batch_size)
        x = self.features(x)
        x=self.frp_layer(x,rects)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        output=[]
        for layer in self.loss_layers:
            attribute=F.softmax(layer(x))
            # pdb.set_trace()
            # max_val,indices=torch.max(attribute,dim=1)
            output.append(attribute)
        
        #softmax for each attribute followed byBinary cross entropy loss in train()
        # pdb.set_trace()
        return torch.cat(output,dim=1)


def alexnet(pretrained=False, **kwargs):
    r"""
    Loads pre-trained imagenet based model weights
    Args:image_height,image_width,num_attributes=None,num_attribute_values=None,classes_set_non_uniform=False, num_classes=29

    
    """
    model = AlexNet_FRP(**kwargs)
    # if pretrained:
    #     model.load_state_dict(model_zoo.load_url(model_urls['alexnet']))
    #     print(model.cuda())
    #     
    return model

# alexnet(pretrained=True)