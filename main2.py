#!/usr/bin/env python

from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
# import matplotlib.pyplot as plt
import time
import os
import copy
from torchviz import make_dot
from alexnet import alexnet
from vgg import vgg11
from dataset import customFaceDataset
import sys
import pdb
import cv2
from tensorboardX import SummaryWriter
writer=SummaryWriter()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

CLASSES={'Gender '       :  ( 'MALE','FEMALE'),
  '  Expressions' :  ('ANGER', 'HAPPINESS', 'SADNESS', 'SURPRISE', 'FEAR', 'DISGUST','NEUTRAL'),
  '  Occlusion  '  :   ('GLASSES',' BEARD', 'ORNAMENTS', 'HAIR', 'HAND',' NONE',' OTHERS'),
  '  Illumination'  :   ('BAD', 'MEDIUM', 'HIGH'),
  '  Age         '    :   ('CHILD', 'YOUNG',' MIDDLE' , 'OLD'),
  '  Pose        '    :   ('FRONTAL','LEFT',' Right','UP', 'DOWN')   ,
  '  Makeup      ' :  ('PARTIALMAKEUP','OVER-MAKEUP')}


def train_model(dataloaders,model, criterion, optimizer, scheduler, PATH_SAVE,num_classes=30,num_epochs=25):
    
    since = time.time()

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    best_acc = 0.0

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train']:
            if phase == 'train':
                scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0
            preds_epoch=[] 
            labels_epoch=[]
            # Iterate over data.
            for idx,(inputs, labels,rects) in enumerate(dataloaders[phase]):
                images=[]
                inputs = inputs.to(device)
                labels = labels.to(device)
                # for i in file_names:
                #     im=cv2.imread(i)

                #     # im=np.expand_dims(im,axis=0)

                #     images.append(im)
                # # images=np.concatenate(images,axis=0)
                # 
                # images = images.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs,rects)
                    preds=outputs
                 
                    # 
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                
                running_loss += loss.item() * inputs.size(0)
                # running_corrects += torch.sum(preds[preds>0.5].long() == labels.data[labels.data==1].long())
                running_corrects+=r_corrects(preds,labels)
                preds_epoch.append(preds.data)
                labels_epoch.append(labels.data)
#Inefficient: leads to incremental memory use
                print("running loss :{} \n running corrects : {}".format(running_loss,running_corrects))

            epoch_loss = running_loss / len(dataloaders[phase])
            epoch_acc = running_corrects.double() / (len(dataloaders[phase])*num_classes)
            writer.add_scalar('data/loss_epoch',epoch_loss,epoch)
            writer.add_scalar('data/epoch_acc',epoch_acc,epoch)
            preds_epoch=torch.cat(preds_epoch,dim=0)
            labels_epoch=torch.cat(labels_epoch,dim=0)
            writer.add_pr_curve('epoch_pr_curve',labels_epoch,preds_epoch,epoch)
            
            for name,params in model.named_parameters():
                writer.add_histogram(name,params.clone().cpu().data.numpy(),epoch)
            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                torch.save(model.state_dict(),PATH_SAVE+os.sep+'epoch_'+str(epoch)+'.pth')

        

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    # print('Best val Acc: {:4f}'.format(best_acc)) for val phase

    # load best model weights with val phase else just the last set of parameter values

    # last_saved=max(os.listdir(PATH_SAVE),key=os.path.getctime)
    # model.load_state_dict(last_saved)

    return model
def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, 'model_best.pth.tar')
         # if args.resume:
#CALL save_checkpoint using the following code from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L139            
        # if os.path.isfile(args.resume):
        #     print("=> loading checkpoint '{}'".format(args.resume))
        #     checkpoint = torch.load(args.resume)
        #     args.start_epoch = checkpoint['epoch']
        #     best_prec1 = checkpoint['best_prec1']
        #     model.load_state_dict(checkpoint['state_dict'])
        #     optimizer.load_state_dict(checkpoint['optimizer'])
        #     print("=> loaded checkpoint '{}' (epoch {})"
        #           .format(args.resume, checkpoint['epoch']))
def test_model(dataloaders, model, criterion,num_epochs=1):
    batch_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        end = time.time()
        for i, (input, target) in enumerate(dataloaders):
            if args.gpu is not None:
                input = input.cuda(args.gpu, non_blocking=True)
            target = target.cuda(args.gpu, non_blocking=True)

            # compute output
            output = model(input)
            loss = criterion(output, target)

            # measure accuracy and record loss
            prec1, prec5 = accuracy(output, target, topk=(1, 5))
            losses.update(loss.item(), input.size(0))
            top1.update(prec1[0], input.size(0))
            top5.update(prec5[0], input.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % args.print_freq == 0:
                print('Test: [{0}/{1}]\t'
                      'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                      'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                      'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
                      'Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
                       i, len(val_loader), batch_time=batch_time, loss=losses,
                       top1=top1, top5=top5))

        print(' * Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f}'
              .format(top1=top1, top5=top5))

    return top1.avg

def accuracy(outputs, targeta):
   
    preds = outputs >= 0.5
    targets = targets >= 0.5
    acc = pred.eq(targets).sum() / targets.numel()
    return acc

def r_corrects(outputs,targets):
    preds = outputs >= 0.5
    targets = targets >= 0.5
    return preds.eq(targets).sum()

def main():
    
    num_epochs=3
    batch_size=8
    PATH_SAVE='checkpoints3'
    mean=[0.448464, 0.341296 ,0.305745]
    std=[0.541134 ,0.508146 ,0.495508]

    data_transforms = {
        'train': transforms.Compose([
            transforms.Resize((245,196)),
            transforms.ToTensor(),
            transforms.Normalize(mean,std )
        ]),
        'val': transforms.Compose([
            transforms.Resize((245,196)),
            transforms.ToTensor(),
            transforms.Normalize(mean,std)
        ]),
    }
    # partial data augmentation implementation
    #further augmentation effects the CascadeClassifier
    PATH='data_196_245.csv'
    train_dataset=customFaceDataset(csv_path=PATH,root='faces3',transform=data_transforms['train'])
    #Partial data augmentation
    train_loader=torch.utils.data.DataLoader(train_dataset,batch_size=batch_size,shuffle=True,num_workers=4)
#set up test_dataset and loader
    loaders={'train':train_loader}
    
    x,y,z=next(iter(train_loader))


    net = alexnet(pretrained=True,batch_size=batch_size,mean=mean,std=std,classes_set_non_uniform=CLASSES)
    
    print(alexnet)

    net = net.to(device)
    out=net(x,z)
    
    criterion = nn.BCELoss()
    los=criterion(out,y)

    
    # k=make_dot(los,params=dict(net.named_parameters()))
    # k.render('FRP_AlexNet',view=True)
    # Observe that all parameters are being optimized
    #F
    optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

    # Decay LR by a factor of 0.1 every 7 epochs
    scheduler = lr_scheduler.StepLR(optimizer, step_size=7, gamma=0.1)


    model=train_model(dataloaders=loaders,model=net,criterion=criterion,optimizer=optimizer,scheduler=scheduler,PATH_SAVE=PATH_SAVE,num_epochs=num_epochs)
    writer.export_scalars_to_json("./all_scalars.json")
    writer.close()
    torch.save(model.state_dict(),PATH_SAVE+os.sep+'checkpoint')



if __name__ == '__main__':
    main()
