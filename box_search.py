#!/usr/bin/env python
import cv2
import dlib
import numpy as  np
import pdb
import os
# from torch.autograd import Function, Variable
# import torch

class FRP_input():
    def __init__(self,landmark_predictor="shape_predictor_68_face_landmarks.dat",haar_features='/home/face_detection/haarcascade_frontalface_default.xml'):
        super(FRP_input,self).__init__()
        self.predictor = dlib.shape_predictor(landmark_predictor)
        
        self.cascade = cv2.CascadeClassifier(haar_features)


        FACE_POINTS = list(range(17, 68))
        MOUTH_POINTS = list(range(48, 61))
        RIGHT_BROW_POINTS = list(range(17, 22))
        LEFT_BROW_POINTS = list(range(22, 27))
        RIGHT_EYE_POINTS = list(range(36, 42))
        LEFT_EYE_POINTS = list(range(42, 48))
        NOSE_POINTS = list(range(27, 35))
        JAW_POINTS = list(range(0, 17))
        CHIN_POINTS=list(range(6,11))
        self.point_sets=[FACE_POINTS,MOUTH_POINTS,RIGHT_BROW_POINTS,LEFT_BROW_POINTS,RIGHT_EYE_POINTS,LEFT_EYE_POINTS,NOSE_POINTS,JAW_POINTS,CHIN_POINTS]



    def get_rects_preload(self,rects_preload,batch_size):
        batch_regions=[]
        regions={}
        for idx in range(rects_preload.shape[0]):
            landmarks=rects_preload[idx,:,:]
            for idx2,j in enumerate(self.point_sets):
                pts=landmarks[j]
                r=self.set_rect(pts.data.numpy())

    
                regions[idx2]=(r[0],r[1],r[0]+r[2],r[1]+r[3])
            batch_regions.append(regions)
        return batch_regions

    def get_rects(self,file_names,batch_size):
        batch_regions=[]
        for i in range(batch_size):
            regions={}
            # 
            name=file_names[i]
            im=cv2.imread(name)
            im=im.resize((196,245))
            print('remember to denormalise')
            # im=image.copy()
            landmarks=self.get_landmarks(im)
            for idx,i in enumerate(self.point_sets):
                pts=landmarks[i]
                r=self.set_rect(pts)

                regions[idx]=(r[0],r[1],r[0]+r[2],r[1]+r[3])

            batch_regions.append(regions)
        return batch_regions

    

    def get_landmarks(self,im):
                
        # im=cv2.cvtColor(im,cv2.COLOR_BGR2RGB)
        # im=np.transpose(im,(1,0,2))
        # 
        rects = self.cascade.detectMultiScale(im, 1.3,5)
        x,y,w,h =rects[0]
        #Remember to denormlaise the np array

        rect=dlib.rectangle(x,y,x+w,y+h)
        return np.matrix([[p.x, p.y] for p in self.predictor(im, rect).parts()])

    def set_rect(self,pts):
        return cv2.boundingRect(pts)



# def main():

# 


# 
# f=FRP()
# im=cv2.imread('stock1.jpeg')
# m=f.get_rects(im)
# im=cv2.imread('stock1.jpeg')
# cv2.imwrite('Result.png',f.annotate_landmarks(im,f.get_landmarks(im)))
# cv2.waitKey(0)
# cv2.imwrite('NOSE_POINTS.png',annotate_landmarks(im,get_landmarks(im),NOSE_POINTS))
