#!/usr/bin/env python

import pandas as pd
import numpy as np
from PIL import Image

import torch
from torch.autograd import Variable
from torch.utils.data.dataset import Dataset
import pdb
import os
import cv2
'''
Annotation: For every image, annotation is provided for following attributes:
	Gender        :   Male, Female 
    Expressions :   Anger, Happiness, Sadness, Surprise, Fear, Disgust,neutral
    Occlusion    :   Glasses, Beard, Ornaments, Hair, Hand, None, Others
    Illumination  :   Bad, Medium, High
    Age             :   Child, Young, Middle and Old
    Pose            :   Frontal, Left, Right, Up, Down   
    Makeup       :   Partial makeup, Over-makeup
    '''
CLASSES={'Gender '       :  ( 'MALE','FEMALE'),
  '  Expressions' :  ('ANGER', 'HAPPINESS', 'SADNESS', 'SURPRISE', 'FEAR', 'DISGUST','NEUTRAL'),
  '  Occlusion  '  :   ('GLASSES',' BEARD', 'ORNAMENTS', 'HAIR', 'HAND',' NONE',' OTHERS'),
  '  Illumination'  :   ('BAD', 'MEDIUM', 'HIGH'),
  '  Age         '    :   ('CHILD', 'YOUNG',' MIDDLE' , 'OLD'),
  '  Pose        '    :   ('FRONTAL','LEFT',' Right','UP', 'DOWN')   ,
  '  Makeup      ' :  ('PARTIALMAKEUP','OVER-MAKEUP')}
class customFaceDataset(Dataset):
    def __init__(self, csv_path,root, transform):
        """
        Args:
            csv_path (string): path to csv file
            height (int): image height
            width (int): image width
            transform: pytorch transforms for transforms and tensor conversion
        """
        self.root=root
        self.data = pd.read_csv(csv_path)
        self.labels = np.asarray(self.data.iloc[:, 1:]).astype(np.float32)
        self.array=np.load('rect_file_path5.npy')
        self.transform = transform
        

    def __getitem__(self, index):
        
        # 
        root=self.root
        file_name=os.path.join(root,self.data.iloc[index][0])
        img = Image.open(file_name)
        # img_as_BGR=cv2.imread(os.path.join(root,self.data.iloc[index][0]))
        # k=[]
        # lengths=[len(i)  for i in CLASSES.values()]
        # idx_last=0
        array_labels=np.array(self.labels[index,:])
        # for i in lengths:
        # 	attribute=array_labels[idx_last:idx_last+i]
        # 	k.append(np.argmax(attribute))
        # 	idx_last+=i
        	
        
        # labels=np.array(k)
        if self.transform is not None:
            img_as_tensor = self.transform(img)
        #need to implement landmark pixel point shaking- data augmentation
        
        return (img_as_tensor, array_labels,self.array[index])

    def __len__(self):
    	return len(self.data)

# a=customFaceDataset(csv_path='data_196_245.csv',root='faces3')

# k=a.__getitem__(5)
