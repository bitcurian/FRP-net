#!/usr/bin/env python



import cv2
import dlib
import os
import numpy as np
import pdb
from glob import glob
PREDICTOR_PATH = "shape_predictor_68_face_landmarks.dat"
predictor = dlib.shape_predictor(PREDICTOR_PATH)
cascade_path='haarcascade_frontalface_default.xml'
cascade = cv2.CascadeClassifier(cascade_path)

# #This is using the Dlib Face Detector . Better result more time taking
# def get_landmarks(im):
#     rects = detector(im, 1)
#     rect=rects[0]
#     print type(rect.width())
#     fwd=int(rect.width())
#     if len(rects) == 0:
#         return None,None

#     return np.matrix([[p.x, p.y] for p in predictor(im, rects[0]).parts()]),fwd

def get_landmarks(im):
    rects = cascade.detectMultiScale(im, 1.3,5)
    if isinstance(rects,np.ndarray):
        x,y,w,h =rects[0]
        rect=dlib.rectangle(x,y,x+w,y+h)
        return np.matrix([[p.x, p.y] for p in predictor(im, rect).parts()])
    else:
        return False
    

def annotate_landmarks(im, landmarks):
    if isinstance(landmarks,np.ndarray):
        
        im = im.copy()
        for idx, point in enumerate(landmarks):
            pos = (point[0, 0], point[0, 1])
            cv2.putText(im, str(idx), pos,
                        fontFace=cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                        fontScale=0.4,
                        color=(0, 0, 255))
            cv2.circle(im, pos, 3, color=(0, 255, 255))
        return im
    else:
        
        return False

# No.->(W,H)
# 3->(196,245)
# 2->(224,224)
files=glob('IMFDB_final/*/*/images/*.jpg')
with open('file_paths3.txt','a') as f:
    lines=[]
    txt_file='faces3'
    array=[]
    if(not(os.path.exists(txt_file))):
                os.mkdir(txt_file)
    for idx,i in enumerate(files):

        im1=cv2.imread(i)
        im=cv2.resize(im1,(196,245))
        # im=cv2.resize(im,(224,224))
        marks=annotate_landmarks(im,get_landmarks(im))
        
        # pdb.set_trace()
    
        if(isinstance(marks,np.ndarray)):

            cv2.imwrite(os.path.join(txt_file,i.split('/')[-1]),im1)
            array.append(get_landmarks(im))
            # if(os.path.exists(os.path.join('faces',str(idx)+'.jpg'))):
            #     os.remove(os.path.join('faces',str(idx)+'.jpg'))
            lines.append(i+'\n')
    f.writelines(lines)
    np.save('rect_file_path3',np.array(array))
# cv2.imshow('Result',annotate_landmarks(im,get_landmarks(im)))
# cv2.waitKey(0)
# cv2.destroyAllWindows()