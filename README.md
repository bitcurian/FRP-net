# FRP-net

Implementation of  "Computational Face Reader based on Facial
Attribute Estimation" authored by Xiangbo Shu, Yunfei Cai, Liu Yang, Liyan Zhang,
Jinhui Tang.

To visualise the plots for accuracy,loss, p_r curves -
Run :
tensorboard --logdir runs

The validation and testing modules haven't been added.
The files in this repository are used for preparing the IMFDB for fine-grained attribute classification and implementing the FRP-AlexNet model from the above paper to the best possible circumstance.
